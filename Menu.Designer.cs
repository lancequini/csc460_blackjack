﻿namespace CSC460_BlackJack
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuTabs = new System.Windows.Forms.TabControl();
            this.menuPage = new System.Windows.Forms.TabPage();
            this.mainProfileName = new System.Windows.Forms.Label();
            this.mainProfileLabel = new System.Windows.Forms.Label();
            this.mainPlayButton = new System.Windows.Forms.Button();
            this.mainTitle = new System.Windows.Forms.Label();
            this.menuBackground = new System.Windows.Forms.PictureBox();
            this.profilePage = new System.Windows.Forms.TabPage();
            this.profileCreateInfo = new System.Windows.Forms.Label();
            this.profileCreateTextBox = new System.Windows.Forms.TextBox();
            this.profileCreateButton = new System.Windows.Forms.Button();
            this.infoPanel = new System.Windows.Forms.Panel();
            this.profileOverallTotal = new System.Windows.Forms.Label();
            this.profileOverallLabel = new System.Windows.Forms.Label();
            this.profileBlackjacks = new System.Windows.Forms.Label();
            this.profileHandsWon = new System.Windows.Forms.Label();
            this.profileWinPct = new System.Windows.Forms.Label();
            this.profileChipCount = new System.Windows.Forms.Label();
            this.profileHandsLabel = new System.Windows.Forms.Label();
            this.profileBJLabel = new System.Windows.Forms.Label();
            this.profileWinLabel = new System.Windows.Forms.Label();
            this.profileChipLabel = new System.Windows.Forms.Label();
            this.profileDelete = new System.Windows.Forms.Button();
            this.profileBox = new System.Windows.Forms.ComboBox();
            this.profileHelpLabel = new System.Windows.Forms.Label();
            this.profileBackground = new System.Windows.Forms.PictureBox();
            this.helpPage = new System.Windows.Forms.TabPage();
            this.helpLabel = new System.Windows.Forms.Label();
            this.helpBackground = new System.Windows.Forms.PictureBox();
            this.menuTabs.SuspendLayout();
            this.menuPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.menuBackground)).BeginInit();
            this.profilePage.SuspendLayout();
            this.infoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profileBackground)).BeginInit();
            this.helpPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helpBackground)).BeginInit();
            this.SuspendLayout();
            // 
            // menuTabs
            // 
            this.menuTabs.Controls.Add(this.menuPage);
            this.menuTabs.Controls.Add(this.profilePage);
            this.menuTabs.Controls.Add(this.helpPage);
            this.menuTabs.Location = new System.Drawing.Point(-3, 1);
            this.menuTabs.Name = "menuTabs";
            this.menuTabs.SelectedIndex = 0;
            this.menuTabs.Size = new System.Drawing.Size(439, 390);
            this.menuTabs.TabIndex = 0;
            // 
            // menuPage
            // 
            this.menuPage.Controls.Add(this.mainProfileName);
            this.menuPage.Controls.Add(this.mainProfileLabel);
            this.menuPage.Controls.Add(this.mainPlayButton);
            this.menuPage.Controls.Add(this.mainTitle);
            this.menuPage.Controls.Add(this.menuBackground);
            this.menuPage.Location = new System.Drawing.Point(4, 22);
            this.menuPage.Name = "menuPage";
            this.menuPage.Padding = new System.Windows.Forms.Padding(3);
            this.menuPage.Size = new System.Drawing.Size(431, 364);
            this.menuPage.TabIndex = 0;
            this.menuPage.Text = "Main Menu";
            this.menuPage.UseVisualStyleBackColor = true;
            // 
            // mainProfileName
            // 
            this.mainProfileName.AutoSize = true;
            this.mainProfileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainProfileName.Location = new System.Drawing.Point(215, 193);
            this.mainProfileName.Name = "mainProfileName";
            this.mainProfileName.Size = new System.Drawing.Size(99, 20);
            this.mainProfileName.TabIndex = 3;
            this.mainProfileName.Text = "Profile Name";
            this.mainProfileName.Parent = menuBackground;
            this.mainProfileName.ForeColor = System.Drawing.Color.White;
            // 
            // mainProfileLabel
            // 
            this.mainProfileLabel.AutoSize = true;
            this.mainProfileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainProfileLabel.Location = new System.Drawing.Point(161, 193);
            this.mainProfileLabel.Name = "mainProfileLabel";
            this.mainProfileLabel.Size = new System.Drawing.Size(57, 20);
            this.mainProfileLabel.TabIndex = 2;
            this.mainProfileLabel.Text = "Profile:";
            this.mainProfileLabel.Parent = menuBackground;
            this.mainProfileLabel.ForeColor = System.Drawing.Color.White;
            // 
            // mainPlayButton
            // 
            this.mainPlayButton.Location = new System.Drawing.Point(177, 233);
            this.mainPlayButton.Name = "mainPlayButton";
            this.mainPlayButton.Size = new System.Drawing.Size(75, 23);
            this.mainPlayButton.TabIndex = 1;
            this.mainPlayButton.Text = "Play";
            this.mainPlayButton.UseVisualStyleBackColor = true;
            this.mainPlayButton.Click += new System.EventHandler(this.mainPlayButton_Click);
            // 
            // mainTitle
            // 
            this.mainTitle.AutoSize = true;
            this.mainTitle.BackColor = System.Drawing.Color.Transparent;
            this.mainTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mainTitle.Location = new System.Drawing.Point(65, 99);
            this.mainTitle.Name = "mainTitle";
            this.mainTitle.Size = new System.Drawing.Size(303, 73);
            this.mainTitle.TabIndex = 0;
            this.mainTitle.Text = "Blackjack";
            this.mainTitle.Parent = menuBackground;
            this.mainTitle.ForeColor = System.Drawing.Color.White;
            // 
            // menuBackground
            // 
            this.menuBackground.Image = global::CSC460_BlackJack.Properties.Resources.Background;
            this.menuBackground.Location = new System.Drawing.Point(0, 0);
            this.menuBackground.Name = "menuBackground";
            this.menuBackground.Size = new System.Drawing.Size(434, 367);
            this.menuBackground.TabIndex = 4;
            this.menuBackground.TabStop = false;
            // 
            // profilePage
            // 
            this.profilePage.Controls.Add(this.profileCreateInfo);
            this.profilePage.Controls.Add(this.profileCreateTextBox);
            this.profilePage.Controls.Add(this.profileCreateButton);
            this.profilePage.Controls.Add(this.infoPanel);
            this.profilePage.Controls.Add(this.profileBox);
            this.profilePage.Controls.Add(this.profileHelpLabel);
            this.profilePage.Controls.Add(this.profileBackground);
            this.profilePage.Location = new System.Drawing.Point(4, 22);
            this.profilePage.Name = "profilePage";
            this.profilePage.Padding = new System.Windows.Forms.Padding(3);
            this.profilePage.Size = new System.Drawing.Size(431, 364);
            this.profilePage.TabIndex = 1;
            this.profilePage.Text = "Profile";
            this.profilePage.UseVisualStyleBackColor = true;
            // 
            // profileCreateInfo
            // 
            this.profileCreateInfo.AutoSize = true;
            this.profileCreateInfo.Location = new System.Drawing.Point(68, 294);
            this.profileCreateInfo.Name = "profileCreateInfo";
            this.profileCreateInfo.Size = new System.Drawing.Size(136, 13);
            this.profileCreateInfo.TabIndex = 5;
            this.profileCreateInfo.Text = "(Use only alpha characters)";
            this.profileCreateInfo.Parent = profileBackground;
            this.profileCreateInfo.ForeColor = System.Drawing.Color.White;
            // 
            // profileCreateTextBox
            // 
            this.profileCreateTextBox.Location = new System.Drawing.Point(12, 317);
            this.profileCreateTextBox.Name = "profileCreateTextBox";
            this.profileCreateTextBox.Size = new System.Drawing.Size(192, 20);
            this.profileCreateTextBox.TabIndex = 4;
            this.profileCreateTextBox.Parent = profileBackground;
            // 
            // profileCreateButton
            // 
            this.profileCreateButton.Location = new System.Drawing.Point(227, 317);
            this.profileCreateButton.Name = "profileCreateButton";
            this.profileCreateButton.Size = new System.Drawing.Size(95, 20);
            this.profileCreateButton.TabIndex = 3;
            this.profileCreateButton.Text = "Create Profile";
            this.profileCreateButton.UseVisualStyleBackColor = true;
            this.profileCreateButton.Click += new System.EventHandler(this.profileCreateButton_Click);
            // 
            // infoPanel
            // 
            this.infoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.infoPanel.Controls.Add(this.profileOverallTotal);
            this.infoPanel.Controls.Add(this.profileOverallLabel);
            this.infoPanel.Controls.Add(this.profileBlackjacks);
            this.infoPanel.Controls.Add(this.profileHandsWon);
            this.infoPanel.Controls.Add(this.profileWinPct);
            this.infoPanel.Controls.Add(this.profileChipCount);
            this.infoPanel.Controls.Add(this.profileHandsLabel);
            this.infoPanel.Controls.Add(this.profileBJLabel);
            this.infoPanel.Controls.Add(this.profileWinLabel);
            this.infoPanel.Controls.Add(this.profileChipLabel);
            this.infoPanel.Controls.Add(this.profileDelete);
            this.infoPanel.Location = new System.Drawing.Point(11, 69);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(411, 181);
            this.infoPanel.TabIndex = 2;
            this.infoPanel.Parent = profileBackground;
            // 
            // profileOverallTotal
            // 
            this.profileOverallTotal.AutoSize = true;
            this.profileOverallTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileOverallTotal.Location = new System.Drawing.Point(123, 42);
            this.profileOverallTotal.Name = "profileOverallTotal";
            this.profileOverallTotal.Size = new System.Drawing.Size(27, 16);
            this.profileOverallTotal.TabIndex = 10;
            this.profileOverallTotal.Text = "OT";
            this.profileOverallTotal.ForeColor = System.Drawing.Color.White;
            // 
            // profileOverallLabel
            // 
            this.profileOverallLabel.AutoSize = true;
            this.profileOverallLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileOverallLabel.Location = new System.Drawing.Point(3, 42);
            this.profileOverallLabel.Name = "profileOverallLabel";
            this.profileOverallLabel.Size = new System.Drawing.Size(88, 16);
            this.profileOverallLabel.TabIndex = 9;
            this.profileOverallLabel.Text = "Overall Total:";
            this.profileOverallLabel.ForeColor = System.Drawing.Color.White;
            // 
            // profileBlackjacks
            // 
            this.profileBlackjacks.AutoSize = true;
            this.profileBlackjacks.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileBlackjacks.Location = new System.Drawing.Point(123, 149);
            this.profileBlackjacks.Name = "profileBlackjacks";
            this.profileBlackjacks.Size = new System.Drawing.Size(24, 16);
            this.profileBlackjacks.TabIndex = 8;
            this.profileBlackjacks.Text = "BJ";
            this.profileBlackjacks.ForeColor = System.Drawing.Color.White;
            // 
            // profileHandsWon
            // 
            this.profileHandsWon.AutoSize = true;
            this.profileHandsWon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileHandsWon.Location = new System.Drawing.Point(122, 112);
            this.profileHandsWon.Name = "profileHandsWon";
            this.profileHandsWon.Size = new System.Drawing.Size(31, 16);
            this.profileHandsWon.TabIndex = 7;
            this.profileHandsWon.Text = "HW";
            this.profileHandsWon.ForeColor = System.Drawing.Color.White;
            // 
            // profileWinPct
            // 
            this.profileWinPct.AutoSize = true;
            this.profileWinPct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileWinPct.Location = new System.Drawing.Point(123, 78);
            this.profileWinPct.Name = "profileWinPct";
            this.profileWinPct.Size = new System.Drawing.Size(30, 16);
            this.profileWinPct.TabIndex = 6;
            this.profileWinPct.Text = "WP";
            this.profileWinPct.ForeColor = System.Drawing.Color.White;
            // 
            // profileChipCount
            // 
            this.profileChipCount.AutoSize = true;
            this.profileChipCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileChipCount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.profileChipCount.Location = new System.Drawing.Point(123, 10);
            this.profileChipCount.Name = "profileChipCount";
            this.profileChipCount.Size = new System.Drawing.Size(26, 16);
            this.profileChipCount.TabIndex = 5;
            this.profileChipCount.Text = "CC";
            this.profileChipCount.ForeColor = System.Drawing.Color.White;
            // 
            // profileHandsLabel
            // 
            this.profileHandsLabel.AutoSize = true;
            this.profileHandsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileHandsLabel.Location = new System.Drawing.Point(2, 112);
            this.profileHandsLabel.Name = "profileHandsLabel";
            this.profileHandsLabel.Size = new System.Drawing.Size(82, 16);
            this.profileHandsLabel.TabIndex = 4;
            this.profileHandsLabel.Text = "Hands Won:";
            this.profileHandsLabel.ForeColor = System.Drawing.Color.White;
            // 
            // profileBJLabel
            // 
            this.profileBJLabel.AutoSize = true;
            this.profileBJLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileBJLabel.Location = new System.Drawing.Point(3, 149);
            this.profileBJLabel.Name = "profileBJLabel";
            this.profileBJLabel.Size = new System.Drawing.Size(77, 16);
            this.profileBJLabel.TabIndex = 3;
            this.profileBJLabel.Text = "Blackjacks:";
            this.profileBJLabel.ForeColor = System.Drawing.Color.White;
            // 
            // profileWinLabel
            // 
            this.profileWinLabel.AutoSize = true;
            this.profileWinLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileWinLabel.Location = new System.Drawing.Point(3, 78);
            this.profileWinLabel.Name = "profileWinLabel";
            this.profileWinLabel.Size = new System.Drawing.Size(56, 16);
            this.profileWinLabel.TabIndex = 2;
            this.profileWinLabel.Text = "Win Pct:";
            this.profileWinLabel.ForeColor = System.Drawing.Color.White;
            // 
            // profileChipLabel
            // 
            this.profileChipLabel.AutoSize = true;
            this.profileChipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileChipLabel.Location = new System.Drawing.Point(3, 10);
            this.profileChipLabel.Name = "profileChipLabel";
            this.profileChipLabel.Size = new System.Drawing.Size(75, 16);
            this.profileChipLabel.TabIndex = 1;
            this.profileChipLabel.Text = "Chip Count:";
            this.profileChipLabel.ForeColor = System.Drawing.Color.White;
            // 
            // profileDelete
            // 
            this.profileDelete.Location = new System.Drawing.Point(331, 153);
            this.profileDelete.Name = "profileDelete";
            this.profileDelete.Size = new System.Drawing.Size(75, 23);
            this.profileDelete.TabIndex = 0;
            this.profileDelete.Text = "Delete";
            this.profileDelete.UseVisualStyleBackColor = true;
            this.profileDelete.Click += new System.EventHandler(this.profileDelete_Click);
            // 
            // profileBox
            // 
            this.profileBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.profileBox.FormattingEnabled = true;
            this.profileBox.Location = new System.Drawing.Point(18, 42);
            this.profileBox.Name = "profileBox";
            this.profileBox.Size = new System.Drawing.Size(189, 21);
            this.profileBox.TabIndex = 1;
            this.profileBox.SelectedIndexChanged += new System.EventHandler(this.profileBox_SelectedIndexChanged);
            // 
            // profileHelpLabel
            // 
            this.profileHelpLabel.AutoSize = true;
            this.profileHelpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.profileHelpLabel.Location = new System.Drawing.Point(17, 14);
            this.profileHelpLabel.Name = "profileHelpLabel";
            this.profileHelpLabel.Size = new System.Drawing.Size(199, 280);
            this.profileHelpLabel.TabIndex = 0;
            this.profileHelpLabel.Text = "Select a profile from below.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nCreate a new profile:";
            this.profileHelpLabel.Parent = profileBackground;
            this.profileHelpLabel.ForeColor = System.Drawing.Color.White;
            // 
            // profileBackground
            // 
            this.profileBackground.BackgroundImage = global::CSC460_BlackJack.Properties.Resources.Background;
            this.profileBackground.Location = new System.Drawing.Point(0, 0);
            this.profileBackground.Name = "profileBackground";
            this.profileBackground.Size = new System.Drawing.Size(435, 368);
            this.profileBackground.TabIndex = 6;
            this.profileBackground.TabStop = false;
            // 
            // helpPage
            // 
            this.helpPage.Controls.Add(this.helpLabel);
            this.helpPage.Controls.Add(this.helpBackground);
            this.helpPage.Location = new System.Drawing.Point(4, 22);
            this.helpPage.Name = "helpPage";
            this.helpPage.Size = new System.Drawing.Size(431, 364);
            this.helpPage.TabIndex = 2;
            this.helpPage.Text = "Help";
            this.helpPage.UseVisualStyleBackColor = true;
            // 
            // helpLabel
            // 
            this.helpLabel.AutoSize = true;
            this.helpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpLabel.Location = new System.Drawing.Point(11, 11);
            this.helpLabel.Name = "helpLabel";
            this.helpLabel.Size = new System.Drawing.Size(264, 144);
            this.helpLabel.TabIndex = 1;
            this.helpLabel.Text = "Rules:\r\n\r\n-Dealer stays on 17+.\r\n-BlackJack pays 1.5 times bet.\r\n-5 cards and 21 " +
    "or less wins.\r\n-Split only on same face value.\r\n";
            this.helpLabel.Parent = helpBackground;
            this.helpLabel.ForeColor = System.Drawing.Color.White;
            // 
            // helpBackground
            // 
            this.helpBackground.Image = global::CSC460_BlackJack.Properties.Resources.Background;
            this.helpBackground.Location = new System.Drawing.Point(0, 0);
            this.helpBackground.Name = "helpBackground";
            this.helpBackground.Size = new System.Drawing.Size(435, 361);
            this.helpBackground.TabIndex = 0;
            this.helpBackground.TabStop = false;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 389);
            this.Controls.Add(this.menuTabs);
            this.Name = "Menu";
            this.Text = "Menu";
            this.menuTabs.ResumeLayout(false);
            this.menuPage.ResumeLayout(false);
            this.menuPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.menuBackground)).EndInit();
            this.profilePage.ResumeLayout(false);
            this.profilePage.PerformLayout();
            this.infoPanel.ResumeLayout(false);
            this.infoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profileBackground)).EndInit();
            this.helpPage.ResumeLayout(false);
            this.helpPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.helpBackground)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl menuTabs;
        private System.Windows.Forms.TabPage menuPage;
        private System.Windows.Forms.TabPage profilePage;
        private System.Windows.Forms.Label mainTitle;
        private System.Windows.Forms.TabPage helpPage;
        private System.Windows.Forms.Label mainProfileLabel;
        private System.Windows.Forms.Button mainPlayButton;
        private System.Windows.Forms.Label profileHelpLabel;
        private System.Windows.Forms.ComboBox profileBox;
        private System.Windows.Forms.Panel infoPanel;
        private System.Windows.Forms.TextBox profileCreateTextBox;
        private System.Windows.Forms.Button profileCreateButton;
        private System.Windows.Forms.Label mainProfileName;
        private System.Windows.Forms.Label profileCreateInfo;
        private System.Windows.Forms.Button profileDelete;
        private System.Windows.Forms.Label profileWinLabel;
        private System.Windows.Forms.Label profileChipLabel;
        private System.Windows.Forms.Label profileBlackjacks;
        private System.Windows.Forms.Label profileHandsWon;
        private System.Windows.Forms.Label profileWinPct;
        private System.Windows.Forms.Label profileChipCount;
        private System.Windows.Forms.Label profileHandsLabel;
        private System.Windows.Forms.Label profileBJLabel;
        private System.Windows.Forms.Label profileOverallTotal;
        private System.Windows.Forms.Label profileOverallLabel;
        private System.Windows.Forms.PictureBox menuBackground;
        private System.Windows.Forms.PictureBox profileBackground;
        private System.Windows.Forms.PictureBox helpBackground;
        private System.Windows.Forms.Label helpLabel;
    }
}