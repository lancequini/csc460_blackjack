﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSC460_BlackJack
{
    public partial class Game : Form
    {
        Player player;
        Deck deck;
        Dealer dealer;
        Menu menu;
        bool playingSplitHand;

        public Game()
        {
            InitializeComponent();

            player = new Player("Lance");


            game();
            
        }

        public Game(Menu menu)
        {
            InitializeComponent();

            this.player = menu.getSelectedPlayer();
            this.menu = menu;
            setLableTransperent(playerName);
            playerName.Text = player.getName();
            
            game();
        }

        public void game()
        {
            deck = new Deck();
            dealer = new Dealer();
            betAmountBox.Items.Add(5);
            betAmountBox.SelectedItem = 5;

            startGame(deck);
        }



        /*
         * Starts a new game.
         */
        private void startGame(Deck deck)
        {
            loadGameData();
            dealCards();
        }

        /**
         * Loads all the game data to start a game.
         */
        private void loadGameData()
        {
            resetGUI();
            resetBetBox();
            player.resetHand();
            dealer.resetHand();
            playingSplitHand = false;
            if (deck.cutLinePassed())
            {
                deck.resetDeck();
            }

            if (betAmountBox.SelectedItem == null)
            {
                player.buyIn();
                betAmountBox.SelectedItem = 5;
                resetBetBox();
            }
            if ((player.getChipCount() - (int)betAmountBox.SelectedItem) < 0)
            {
                player.buyIn();
            }

            player.placeBet((int)betAmountBox.SelectedItem);
            chipBetAmount.Image = getBetChip((int)betAmountBox.SelectedItem);
            chipBetAmount.Parent = backgroundImage;
            chipBetAmount.BackColor = Color.Transparent;
            setLableTransperent(chipCount);
            chipCount.Text = "Chip Count: " + player.getChipCount();
        }

        private Image getBetChip(int betAmount)
        {
            String filepath = System.Reflection.Assembly.GetEntryAssembly().Location + @"\..\..\..\Blackjack\Blackjack\Chips\";

            switch (betAmount)
            {
                case 5:
                    filepath = filepath + "5";
                    break;
                case 25:
                    filepath = filepath + "25";
                    break;
                case 100:
                    filepath = filepath + "100";
                    break;
                case 500:
                    filepath = filepath + "500";
                    break;
                case 1000:
                    filepath = filepath + "1000";
                    break;
                case 10000:
                    filepath = filepath + "10000";
                    break;
                default:
                    break;
            }

            filepath = filepath + ".png";

            return Image.FromFile(filepath);
        }

        /*
         * Resets the GUI for a new game.
         */
        private void resetGUI()
        {

            chipBetAmount.Location = new Point(260, 242);
            chipBetSplit.Visible = false;


            playerTotal.Location = new Point(260, 305);
            playerCard1.Image = null;
            playerCard1.Visible = false;
            playerCard1.Location = new Point(260, 333);
            playerCard2.Image = null;
            playerCard2.Visible = false;
            playerCard2.Location = new Point(295, 333);
            playerCard3.Image = null;
            playerCard3.Visible = false;
            playerCard3.Location = new Point(330, 333);
            playerCard4.Image = null;
            playerCard4.Visible = false;
            playerCard4.Location = new Point(365, 333);
            playerCard5.Image = null;
            playerCard5.Visible = false;
            playerCard5.Location = new Point(390, 333);
            

            splitTotalLabel.Visible = false;
            playerSplit1.Image = null;
            playerSplit1.Visible = false;
            playerSplit5.Image = null;
            playerSplit5.Visible = false;
            playerSplit2.Image = null;
            playerSplit2.Visible = false;
            playerSplit3.Image = null;
            playerSplit3.Visible = false;
            playerSplit4.Image = null;
            playerSplit4.Visible = false;

            dealerCard1.Image = null;
            dealerCard1.Visible = false;
            dealerCard2.Image = null;
            dealerCard2.Visible = false;
            dealerCard3.Image = null;
            dealerCard3.Visible = false;
            dealerCard4.Image = null;
            dealerCard4.Visible = false;
            dealerCard5.Image = null;
            dealerCard5.Visible = false;
        }


        private void resetBetBox()
        {
            int selectedItem;
            if (betAmountBox.SelectedItem == null)
            {
                selectedItem = 5;
            }
            else
            {
                selectedItem = (int)betAmountBox.SelectedItem;
            }
                
            
            betAmountBox.Items.Clear();
            if(player.getChipCount() > 0)
                betAmountBox.Items.Add(5);
            if (player.getChipCount() > 25)
                betAmountBox.Items.Add(25);
            if (player.getChipCount() > 100)
                betAmountBox.Items.Add(100);
            if (player.getChipCount() > 500)
                betAmountBox.Items.Add(500);
            if (player.getChipCount() > 1000)
                betAmountBox.Items.Add(1000);
            if (player.getChipCount() > 10000)
                betAmountBox.Items.Add(10000);

            if (!betAmountBox.Items.Contains(selectedItem))
            {
                selectedItem = 5;
            }
            betAmountBox.SelectedItem = selectedItem;
        }


        /*
         * Deals the first 4 cards to the dealer and player.
         */
        private void dealCards()
        {
            player.addCard(deck.drawCard());
            dealer.addCard(deck.drawCard());
            player.addCard(deck.drawCard());
            dealer.addCard(deck.drawCard());

            playerCard1.Visible = true;
            playerCard1.Image = player.getHand()[0].GetCardImage();
            playerCard2.Visible = true;
            playerCard2.Image = player.getHand()[1].GetCardImage();
            setLableTransperent(playerTotal);
            playerTotal.Text = "Total: " + player.getHandTotal().ToString();

            dealerCard1.Visible = true;
            dealerCard1.Image = Image.FromFile(System.Reflection.Assembly.GetEntryAssembly().Location + @"\..\..\..\Blackjack\Blackjack\Cards\back.png");
            dealerCard2.Visible = true;
            dealerCard2.Image = dealer.getHand()[1].GetCardImage();

            setLableTransperent(dealerTotal);
            dealerTotal.Text = "Total: " + dealer.getHand()[1].getCardValue().ToString();

            if (player.getHandTotal() == 21)
            {
                int betAmount = player.getBet();
                player.addWin((betAmount * 2) + (betAmount / 2));
                player.addBlackjack();
                endGame();
            }
            else if (player.getHand()[0].getCardValueString() == player.getHand()[1].getCardValueString())
            {
                splitButton.Visible = true;
            }
        }

        private void splitHand()
        {
            playerCard1.Visible = true;
            playerCard1.Image = player.getHand()[0].GetCardImage();
            playerCard1.Location = new Point(115, 333);
            playerCard2.Visible = true;
            playerCard2.Image = player.getHand()[1].GetCardImage();
            playerCard2.Location = new Point(140, 333);

            playerSplit1.Visible = true;
            playerSplit1.Image = player.getSplitHand()[0].GetCardImage();
            playerSplit2.Visible = true;
            playerSplit2.Image = player.getSplitHand()[1].GetCardImage();
            playerSplit2.BringToFront();

            setLableTransperent(playerTotal);
            playerTotal.Text = "Total: " + player.getHandTotal().ToString();
            playerTotal.Location = new Point(115, 305);

            chipBetAmount.Location = new Point(115, 242);
            chipBetSplit.Visible = true;
            chipBetSplit.Parent = backgroundImage;
            chipBetSplit.BackColor = Color.Transparent;
            chipBetSplit.Image = getBetChip((int)betAmountBox.SelectedItem);
        }

        /*
         * Begins the player's turn.
         */
        private void checkPlayerFinished(Boolean playerIsDone)
        {
            if (!isPlayingSplit())
            {
                if (player.getHandTotal() > 21)
                {
                    player.addLoss();
                    endGame();
                }
                if (player.getHandTotal() < 21 && player.getHand().Count == 5)
                {
                    player.addWin(player.getBet() * 2);
                    endGame();
                }
                if (playerIsDone && !(player.getHandTotal() > 21))
                {
                    setLableTransperent(dealerTotal);
                    dealerTotal.Text = "Total: " + dealer.getHandTotal().ToString();
                    dealerTurn();
                }
            }
            else
            {
                if ((player.getSplitHandTotal() > 21) && (player.getHandTotal() > 21))
                {
                    player.addLoss();
                    endGame();
                    playingSplitHand = false;
                }
                if (player.getHandTotal() == 21 && player.getHand().Count == 2)
                {
                    player.addWin(player.getBet() + (player.getBet() / 2));
                    playingSplitHand = true;
                    displayTotals();
                }
                if (player.getSplitHandTotal() == 21 && player.getSplitHand().Count == 2 && playingSplitHand)
                {
                    player.addWin(player.getBet() + (player.getBet() / 2));
                    playingSplitHand = false;
                    dealerTurn();
                    displayTotals();
                    return;
                }
                if (playingSplitHand)
                {
                    if (player.getSplitHandTotal() > 21)
                    {
                        player.addLoss();
                        playingSplitHand = false;
                        dealerTurn();
                        endGame();
                        return;
                    }
                    if (player.getSplitHandTotal() <= 21 && player.getSplitHand().Count == 5)
                    {
                        player.addWin(player.getBet() * 2);
                        playingSplitHand = false;
                        endGame();
                        return;
                    }
                    if (playerIsDone && !(player.getSplitHandTotal() > 21))
                    {
                        setLableTransperent(dealerTotal);
                        dealerTotal.Text = "Total: " + dealer.getHandTotal().ToString();
                        dealerTurn();
                        return;
                    }
                }
                else
                {
                    if (player.getHandTotal() > 21)
                    {
                        player.addLoss();
                        playingSplitHand = true;
                        displayTotals();
                    }
                    if (player.getHandTotal() < 21 && player.getHand().Count == 5)
                    {
                        player.addWin(player.getBet() * 2);
                        playingSplitHand = true;
                        displayTotals();
                        return;
                    }
                    if (playerIsDone && !(player.getHandTotal() > 21))
                    {
                        playingSplitHand = true;
                        return;
                    }
                }
            }
            
            
        }


        /*
         * Implements the dealer's turn.
         */
        private void dealerTurn(){
            while (dealer.getHandTotal() <= 16)
            {
                dealer.addCard(deck.drawCard());
            }
            if (dealer.getHandTotal() > 21)
            {
                player.addWin(player.getBet() * 2);
                if (isPlayingSplit())
                {
                    if (player.getHandTotal() < 21 && player.getSplitHandTotal() < 21)
                    {
                        player.addWin(player.getBet() * 2);
                    }
                }
                endGame();
            }
            else if(!isPlayingSplit())
            {
                if (dealer.getHandTotal() > player.getHandTotal())
                {
                    player.addLoss();
                    endGame();
                }
                if(player.getHandTotal() > dealer.getHandTotal())
                {
                    player.addWin(player.getBet() * 2);
                    endGame();
                }
                if(player.getHandTotal() == dealer.getHandTotal())
                {
                    player.addWin(player.getBet());
                    endGame();
                }
            }
            else if (player.getSplitHandTotal() >= 2)
            {
                if (playingSplitHand)
                {
                    if (dealer.getHandTotal() > 21 && player.getHandTotal() < 21 && player.getHandTotal() < 21)
                    {
                        player.addWin(player.getBet() * 2);
                        endGame();
                    }
                    else
                    {
                        if (dealer.getHandTotal() > player.getSplitHandTotal())
                        {
                            player.addLoss();
                            endGame();
                        }
                        if (player.getSplitHandTotal() > dealer.getHandTotal())
                        {
                            player.addWin(player.getBet() * 2);
                            endGame();
                        }
                        if (player.getSplitHandTotal() == dealer.getHandTotal())
                        {
                            player.addWin(player.getBet());
                            endGame();
                        }
                    }
                }
            }
            displayHands();
            dealerCard1.Image = dealer.getHand()[0].GetCardImage();
        }


        /*
         * Displays the hands.
         */
        private void displayHands()
        {
            displayTotals();

            if (player.getHand().Count >= 3)
            {
                playerCard3.Visible = true;
                playerCard3.Image = player.getHand()[2].GetCardImage();
                if (isPlayingSplit())
                {
                    playerCard3.Location = new Point(175, 333);
                }
                
                playerCard3.BringToFront();
            }
            if (player.getHand().Count >= 4)
            {
                playerCard4.Visible = true;
                playerCard4.Image = player.getHand()[3].GetCardImage();
                if (isPlayingSplit())
                {
                    playerCard4.Location = new Point(210, 333);
                }
                
                playerCard4.BringToFront();
            }
            if (player.getHand().Count >= 5)
            {
                playerCard5.Visible = true;
                playerCard5.Image = player.getHand()[4].GetCardImage();
                if (isPlayingSplit())
                {
                    playerCard5.Location = new Point(245, 333);
                }
                playerCard5.BringToFront();
            }

            if (player.getSplitHand().Count >= 3)
            {
                playerSplit3.Visible = true;
                playerSplit3.Image = player.getSplitHand()[2].GetCardImage();
                playerSplit3.BringToFront();
            }
            if (player.getSplitHand().Count >= 4)
            {
                playerSplit4.Visible = true;
                playerSplit4.Image = player.getSplitHand()[3].GetCardImage();
                playerSplit4.BringToFront();
            }
            if (player.getSplitHand().Count >= 5)
            {
                playerSplit5.Visible = true;
                playerSplit5.Image = player.getSplitHand()[4].GetCardImage();
                playerSplit5.BringToFront();
            }

            if (dealer.getHand().Count >= 3)
            {
                dealerCard3.Visible = true;
                dealerCard3.Image = dealer.getHand()[2].GetCardImage();
                dealerCard3.BringToFront();
                setLableTransperent(dealerTotal);
                dealerTotal.Text = "Total: " + dealer.getHandTotal().ToString();
            } 
            if (dealer.getHand().Count >= 4)
            {
                dealerCard4.Visible = true;
                dealerCard4.Image = dealer.getHand()[3].GetCardImage();
                dealerCard4.BringToFront();
            }
            if (dealer.getHand().Count >= 5)
            {
                dealerCard5.Visible = true;
                dealerCard5.BringToFront();
                dealerCard5.Image = dealer.getHand()[4].GetCardImage();
            }
        }


        /*
         * Checks to see weather the player is playing a split hand.
         */
        private bool isPlayingSplit()
        {
            if (player.getSplitHand().Count < 2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void displayTotals()
        {
            if (!isPlayingSplit())
            {
                setLableTransperent(playerTotal);
                playerTotal.Text = "Total: " + player.getHandTotal().ToString();
                setLableTransperent(splitTotalLabel);
                splitTotalLabel.Text = "Total: " + player.getSplitHandTotal().ToString();
            }
            else
            {
                if (playingSplitHand)
                {
                    setLableTransperent(playerTotal);
                    playerTotal.Text = "Total: " + player.getHandTotal().ToString();
                    setLableTransperent(splitTotalLabel);
                    splitTotalLabel.Text = ">Total: " + player.getSplitHandTotal().ToString();
                }
                else
                {
                    setLableTransperent(playerTotal);
                    playerTotal.Text = ">Total: " + player.getHandTotal().ToString();
                    setLableTransperent(splitTotalLabel);
                    splitTotalLabel.Text = "Total: " + player.getSplitHandTotal().ToString();
                }
            }
        }

        private void hitButton_Click(object sender, EventArgs e)
        {
            if (!playingSplitHand)
            {
                player.addCard(deck.drawCard());
                removeExtraButtons();
            }
            if(playingSplitHand)
            {
                player.addSplitCard(deck.drawCard());
            }
            

            displayHands();

            checkPlayerFinished(false);
        }

        private void buttonDoubleDown_Click(object sender, EventArgs e)
        {
            if ((player.getBet() * 2) < player.getChipCount())
            {
                player.addCard(deck.drawCard());
                player.doubleDown();

                displayHands();

                checkPlayerFinished(true);
            }
        }

        private void splitButton_Click(object sender, EventArgs e)
        {
            player.playerSplit();
            player.addCard(deck.drawCard());
            player.addSplitCard(deck.drawCard());

            splitHand();
            splitTotalLabel.Visible = true;

            displayTotals();
            setLableTransperent(chipCount);
            chipCount.Text = "Chip Count: " + player.getChipCount().ToString();

            removeExtraButtons();
        }

        private void stayButton_Click(object sender, EventArgs e)
        {
            if (!isPlayingSplit())
            {
                deactivateButtons();
                displayHands();
                checkPlayerFinished(true);
            }
            else
            {
                displayHands();
                if (!playingSplitHand)
                {
                    checkPlayerFinished(true);
                }
                else
                {
                    deactivateButtons();
                    checkPlayerFinished(true);
                }
            }
            displayTotals();
        }

        /*
         * Removes the buttons so the player can not use them.
         */
        private void deactivateButtons()
        {
            hitButton.Visible = false;
            stayButton.Visible = false;
            newGameButton.Visible = true;
            doubleDownButton.Visible = false;
            splitButton.Visible = false;
        }

        /*
         * Shows the buttons for the player to use.
         */
        private void activateButtons()
        {
            hitButton.Visible = true;
            stayButton.Visible = true;
            newGameButton.Visible = false;
            doubleDownButton.Visible = true;
            splitButton.Visible = false;
        }

        private void removeExtraButtons()
        {
            doubleDownButton.Visible = false;
            splitButton.Visible = false;
        }

        private void newGameButton_Click(object sender, EventArgs e)
        {
            activateButtons();
            startGame(deck);
        }

        /*
         * Used to show the game is over.
         */
        private void endGame()
        {
            deactivateButtons();
            setLableTransperent(chipCount);
            chipCount.Text = "Chip Count: " + player.getChipCount().ToString();
            menu.updatePlayerInfo(player);
        }

        /*
         * Make Labels Transperent.
         */
        public void setLableTransperent(Label label)
        {
            label.Parent = backgroundImage;
            label.BackColor = Color.Transparent;
           
        }

       
    }
}
