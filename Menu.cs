﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace CSC460_BlackJack
{
    public partial class Menu : Form
    {
        List<Player> playerList = new List<Player>();
 
        public Menu()
        {
            InitializeComponent();

            loadProfiles();
            
            if(profileBox.Items.Count > 0)
                profileBox.SelectedIndex = 0;

            setLabelTransperent(profileHelpLabel);
            setLabelTransperent(profileChipCount);
            setLabelTransperent(mainTitle);
            setLabelTransperent(mainProfileLabel);
            
            setLabelTransperent(mainProfileName);
        }

        private void profileCreateButton_Click(object sender, EventArgs e)
        {
            if (profileCreateTextBox.Text != "")
            {
                String name = profileCreateTextBox.Text;
                
                Regex regex = new Regex("^[a-zA-Z]*$");
                if (regex.IsMatch(name)) {
                    Console.Out.WriteLine("\"" + name + "\"");
                    Player newPlayer = new Player(name);
                    playerList.Add(newPlayer);
                    updateProfileList();
                    saveProfiles();
                    profileBox.SelectedIndex = playerList.Count - 1;
                    profileCreateTextBox.Clear();
                }
            }
        }

        /*
         * Saves all the profiles to the Profiles.dat file.
         */
        private void saveProfiles()
        {
            //string resourceFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string resourceFolder = System.Reflection.Assembly.GetEntryAssembly().Location + @"\..\..\..\Resources\";
            string outputFile = resourceFolder + "Profiles.bin";

            try
            {
                /*
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(outputFile))
                {
                    foreach (Player player in playerList)
                    {
                        file.WriteLine(player.getName() + "|" +
                                        player.getChipCount() + "|" +
                                        player.getOverallTotal() + "|" +
                                        player.getWins() + "|" +
                                        player.getLosses() + "|" +
                                        player.getBlackJacks());
                    }
                }
                 */
                IFormatter iformatter = new BinaryFormatter();
                using (Stream stream = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    iformatter.Serialize(stream, playerList);
                    stream.Close();
                }
            }
            catch (IOException e)
            {
                Console.Out.WriteLine(e.StackTrace);
            }
        }

        /*
         * Loads all the profiles from the Profiles.dat file. If the file does not 
         * exist then one is created.
         */
        private void loadProfiles()
        {
            //string resourceFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string resourceFolder = System.Reflection.Assembly.GetEntryAssembly().Location + @"\..\..\..\Resources\";
            string inputFile = resourceFolder + "Profiles.bin";
            Console.Out.WriteLine(inputFile);

            if (!File.Exists(inputFile))
            {
                StreamWriter sw = File.CreateText(inputFile);
                //File.SetAttributes(inputFile, FileAttributes.Hidden);
                sw.Close();
                createDefaultPlayer(inputFile);
                updateProfileList();
            }
            else
            {
                /*
                String line;
                
                System.IO.StreamReader file = new System.IO.StreamReader(inputFile);
                
                while ((line = file.ReadLine()) != null)
                {
                    
                    string[] playerInfo = line.Split('|');

                    Player player = new Player(playerInfo[0], 
                                                int.Parse(playerInfo[1]), 
                                                int.Parse(playerInfo[2]), 
                                                int.Parse(playerInfo[3]), 
                                                int.Parse(playerInfo[4]), 
                                                int.Parse(playerInfo[5]));

                    playerList.Add(player);

                    profileBox.Items.Add(player.getName());
                }
                file.Close();*/

                try
                {
                    IFormatter formatter = new BinaryFormatter();
                    using (Stream stream = new FileStream(inputFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        if (stream.Length.Equals(0))
                        {
                            stream.Close();
                            return;
                        }
                        playerList = (List<Player>)formatter.Deserialize(stream);
                        stream.Close();
                    }
                }
                catch (IOException e)
                {
                    Console.Out.WriteLine(e.StackTrace);
                }

                if (playerList.Count == 0)
                {
                    createDefaultPlayer(inputFile);
                }
                
                updateProfileList();
            }
        }

        /*
         * Creates a default player and loads it into the file.
         */
        private void createDefaultPlayer(String inputFile)
        {
            if (inputFile == null)
            {
                //string resourceFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string resourceFolder = System.Reflection.Assembly.GetEntryAssembly().Location + @"\..\..\..\Resources\";
                inputFile = resourceFolder + "Profiles.bin";
            }

            playerList.Add(new Player("Player", 100, 0, 0, 0, 0, 100));

            /*
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(inputFile))
            {
               
                foreach (Player player in playerList)
                {
                    file.WriteLine(player.getName() + "|" +
                                    player.getChipCount() + "|" +
                                    player.getOverallTotal() + "|" +
                                    player.getWins() + "|" +
                                    player.getLosses() + "|" +
                                    player.getBlackJacks());
                }
            }
             */

            IFormatter iformatter = new BinaryFormatter();
            using (Stream stream = new FileStream(inputFile, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                iformatter.Serialize(stream, playerList);
                stream.Close();
            }
        }

        /*
         * Updates the list of profiles in the dropdown box on the profile page.
         */
        private void updateProfileList()
        {
            profileBox.Items.Clear();
            foreach(Player player in playerList){
                profileBox.Items.Add(player.getName());
            }
        }

        private void profileDelete_Click(object sender, EventArgs e)
        {
            Player temp = null;

            foreach (Player player in playerList)
            {
                try
                {
                    profileBox.SelectedItem.ToString();
                }
                catch (Exception exce)
                {
                    Console.Out.WriteLine(exce);
                    profileBox.SelectedIndex = 0;
                    break;
                }
                if (player.getName() == profileBox.SelectedItem.ToString())
                {
                    temp = player;
                }
            }

            if (temp != null)
            {
                playerList.Remove(temp);
            }

            if (playerList.Count == 0)
            {
                createDefaultPlayer(null);
            }
            updateProfileList();
            saveProfiles();
            profileBox.SelectedIndex = 0;
        }

        private void profileBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                mainProfileName.Text = profileBox.SelectedItem.ToString();
            } catch(Exception ex){
                Console.Out.WriteLine(ex.StackTrace);
                profileBox.SelectedIndex = 0;
                mainProfileName.Text = profileBox.SelectedItem.ToString();
            }

            updateStatsBox(getSelectedPlayer());
        }


        /*
         * Returns the currently selected player in the profile window.
         */
        public Player getSelectedPlayer()
        {
            Player tempPlayer = null;
            foreach (Player player in playerList)
            {
                try { 
                    if (player.getName() == profileBox.SelectedItem.ToString())
                    {
                        tempPlayer = player;
                    }
                }catch(Exception e){
                    Console.Out.WriteLine(e.StackTrace);
                    profileBox.SelectedIndex = 0;
                    if (player.getName() == profileBox.SelectedItem.ToString())
                    {
                        tempPlayer = player;
                    }
                }
                
            }

            return tempPlayer;
        }

        /*
         * Updates the Stats box for the currently selected player
         */

        private void updateStatsBox(Player player)
        {
            profileChipCount.Text = player.getChipCount().ToString();
            profileOverallTotal.Text = player.getOverallTotal().ToString();
            profileWinPct.Text = player.getWinPercentage().ToString();
            profileHandsWon.Text = player.getWins().ToString();
            profileBlackjacks.Text = player.getBlackJacks().ToString();
            profileBox.SelectedItem = player.getName();
        }

        /*
         * Updates the player info from the game.
         */
        public void updatePlayerInfo(Player player)
        {
            for (int i = 0; i < playerList.Count; i++)
            {
                if (playerList[i].getName() == player.getName())
                {
                    playerList[i] = player;
                }
            }

            updateStatsBox(player);

            saveProfiles();
            updateProfileList();
        }


        /*
         * Starts a new instance of a game
         */
        private void mainPlayButton_Click(object sender, EventArgs e)
        {
            Game newGame = new Game(this);
            newGame.Show();

            profileBox.SelectedItem = getSelectedPlayer();
            updateStatsBox(getSelectedPlayer());
        }

        /*
         *Setup lebelbox transperent
         */
        public void setLabelTransperent(Label label)
        {
            label.BackColor = Color.Transparent;
        }
    }
}
