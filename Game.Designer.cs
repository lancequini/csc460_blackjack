﻿namespace CSC460_BlackJack
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hitButton = new System.Windows.Forms.Button();
            this.stayButton = new System.Windows.Forms.Button();
            this.playerTotal = new System.Windows.Forms.Label();
            this.dealerTotal = new System.Windows.Forms.Label();
            this.newGameButton = new System.Windows.Forms.Button();
            this.chipCount = new System.Windows.Forms.Label();
            this.betAmountBox = new System.Windows.Forms.ComboBox();
            this.playerName = new System.Windows.Forms.Label();
            this.doubleDownButton = new System.Windows.Forms.Button();
            this.splitButton = new System.Windows.Forms.Button();
            this.splitTotalLabel = new System.Windows.Forms.Label();
            this.playerSplit5 = new System.Windows.Forms.PictureBox();
            this.playerSplit4 = new System.Windows.Forms.PictureBox();
            this.playerSplit3 = new System.Windows.Forms.PictureBox();
            this.playerSplit2 = new System.Windows.Forms.PictureBox();
            this.playerSplit1 = new System.Windows.Forms.PictureBox();
            this.playerCard5 = new System.Windows.Forms.PictureBox();
            this.playerCard4 = new System.Windows.Forms.PictureBox();
            this.playerCard3 = new System.Windows.Forms.PictureBox();
            this.playerCard2 = new System.Windows.Forms.PictureBox();
            this.playerCard1 = new System.Windows.Forms.PictureBox();
            this.dealerCard5 = new System.Windows.Forms.PictureBox();
            this.dealerCard4 = new System.Windows.Forms.PictureBox();
            this.dealerCard3 = new System.Windows.Forms.PictureBox();
            this.dealerCard2 = new System.Windows.Forms.PictureBox();
            this.dealerCard1 = new System.Windows.Forms.PictureBox();
            this.backgroundImage = new System.Windows.Forms.PictureBox();
            this.chipBetAmount = new System.Windows.Forms.PictureBox();
            this.chipBetSplit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chipBetAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chipBetSplit)).BeginInit();
            this.SuspendLayout();
            // 
            // hitButton
            // 
            this.hitButton.Location = new System.Drawing.Point(12, 296);
            this.hitButton.Name = "hitButton";
            this.hitButton.Size = new System.Drawing.Size(83, 23);
            this.hitButton.TabIndex = 0;
            this.hitButton.Text = "Hit";
            this.hitButton.UseVisualStyleBackColor = true;
            this.hitButton.Click += new System.EventHandler(this.hitButton_Click);
            // 
            // stayButton
            // 
            this.stayButton.Location = new System.Drawing.Point(12, 325);
            this.stayButton.Name = "stayButton";
            this.stayButton.Size = new System.Drawing.Size(83, 23);
            this.stayButton.TabIndex = 1;
            this.stayButton.Text = "Stay";
            this.stayButton.UseVisualStyleBackColor = true;
            this.stayButton.Click += new System.EventHandler(this.stayButton_Click);
            // 
            // playerTotal
            // 
            this.playerTotal.AutoSize = true;
            this.playerTotal.BackColor = System.Drawing.Color.Transparent;
            this.playerTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.playerTotal.Location = new System.Drawing.Point(115, 305);
            this.playerTotal.Name = "playerTotal";
            this.playerTotal.Size = new System.Drawing.Size(108, 24);
            this.playerTotal.TabIndex = 5;
            this.playerTotal.Text = "Player Total";
            // 
            // dealerTotal
            // 
            this.dealerTotal.AutoSize = true;
            this.dealerTotal.BackColor = System.Drawing.Color.White;
            this.dealerTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dealerTotal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.dealerTotal.Location = new System.Drawing.Point(292, 168);
            this.dealerTotal.Name = "dealerTotal";
            this.dealerTotal.Size = new System.Drawing.Size(111, 24);
            this.dealerTotal.TabIndex = 13;
            this.dealerTotal.Text = "Dealer Total";
            // 
            // newGameButton
            // 
            this.newGameButton.Location = new System.Drawing.Point(12, 354);
            this.newGameButton.Name = "newGameButton";
            this.newGameButton.Size = new System.Drawing.Size(83, 23);
            this.newGameButton.TabIndex = 14;
            this.newGameButton.Text = "Play Again";
            this.newGameButton.UseVisualStyleBackColor = true;
            this.newGameButton.Visible = false;
            this.newGameButton.Click += new System.EventHandler(this.newGameButton_Click);
            // 
            // chipCount
            // 
            this.chipCount.AutoSize = true;
            this.chipCount.BackColor = System.Drawing.Color.Transparent;
            this.chipCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chipCount.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.chipCount.Location = new System.Drawing.Point(100, 496);
            this.chipCount.Name = "chipCount";
            this.chipCount.Size = new System.Drawing.Size(104, 24);
            this.chipCount.TabIndex = 15;
            this.chipCount.Text = "Chip Count";
            // 
            // betAmountBox
            // 
            this.betAmountBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.betAmountBox.FormattingEnabled = true;
            this.betAmountBox.Items.AddRange(new object[] {
            "5",
            "10",
            "25"});
            this.betAmountBox.Location = new System.Drawing.Point(12, 496);
            this.betAmountBox.Name = "betAmountBox";
            this.betAmountBox.Size = new System.Drawing.Size(82, 21);
            this.betAmountBox.TabIndex = 16;
            // 
            // playerName
            // 
            this.playerName.AutoSize = true;
            this.playerName.BackColor = System.Drawing.Color.Transparent;
            this.playerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerName.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.playerName.Location = new System.Drawing.Point(100, 475);
            this.playerName.Name = "playerName";
            this.playerName.Size = new System.Drawing.Size(98, 20);
            this.playerName.TabIndex = 18;
            this.playerName.Text = "Player Name";
            // 
            // doubleDownButton
            // 
            this.doubleDownButton.Location = new System.Drawing.Point(12, 383);
            this.doubleDownButton.Name = "doubleDownButton";
            this.doubleDownButton.Size = new System.Drawing.Size(83, 23);
            this.doubleDownButton.TabIndex = 29;
            this.doubleDownButton.Text = "Double Down";
            this.doubleDownButton.UseVisualStyleBackColor = true;
            this.doubleDownButton.Click += new System.EventHandler(this.buttonDoubleDown_Click);
            // 
            // splitButton
            // 
            this.splitButton.Location = new System.Drawing.Point(12, 412);
            this.splitButton.Name = "splitButton";
            this.splitButton.Size = new System.Drawing.Size(83, 23);
            this.splitButton.TabIndex = 30;
            this.splitButton.Text = "Split";
            this.splitButton.UseVisualStyleBackColor = true;
            this.splitButton.Visible = false;
            this.splitButton.Click += new System.EventHandler(this.splitButton_Click);
            // 
            // splitTotalLabel
            // 
            this.splitTotalLabel.AutoSize = true;
            this.splitTotalLabel.BackColor = System.Drawing.Color.Transparent;
            this.splitTotalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitTotalLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitTotalLabel.Location = new System.Drawing.Point(379, 306);
            this.splitTotalLabel.Name = "splitTotalLabel";
            this.splitTotalLabel.Size = new System.Drawing.Size(108, 24);
            this.splitTotalLabel.TabIndex = 36;
            this.splitTotalLabel.Text = "Player Total";
            // 
            // playerSplit5
            // 
            this.playerSplit5.Location = new System.Drawing.Point(519, 333);
            this.playerSplit5.Name = "playerSplit5";
            this.playerSplit5.Size = new System.Drawing.Size(100, 140);
            this.playerSplit5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerSplit5.TabIndex = 32;
            this.playerSplit5.TabStop = false;
            // 
            // playerSplit4
            // 
            this.playerSplit4.Location = new System.Drawing.Point(484, 333);
            this.playerSplit4.Name = "playerSplit4";
            this.playerSplit4.Size = new System.Drawing.Size(100, 140);
            this.playerSplit4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerSplit4.TabIndex = 35;
            this.playerSplit4.TabStop = false;
            // 
            // playerSplit3
            // 
            this.playerSplit3.Location = new System.Drawing.Point(449, 333);
            this.playerSplit3.Name = "playerSplit3";
            this.playerSplit3.Size = new System.Drawing.Size(100, 140);
            this.playerSplit3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerSplit3.TabIndex = 34;
            this.playerSplit3.TabStop = false;
            // 
            // playerSplit2
            // 
            this.playerSplit2.Location = new System.Drawing.Point(414, 333);
            this.playerSplit2.Name = "playerSplit2";
            this.playerSplit2.Size = new System.Drawing.Size(100, 140);
            this.playerSplit2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerSplit2.TabIndex = 33;
            this.playerSplit2.TabStop = false;
            // 
            // playerSplit1
            // 
            this.playerSplit1.Location = new System.Drawing.Point(379, 333);
            this.playerSplit1.Name = "playerSplit1";
            this.playerSplit1.Size = new System.Drawing.Size(100, 140);
            this.playerSplit1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerSplit1.TabIndex = 31;
            this.playerSplit1.TabStop = false;
            // 
            // playerCard5
            // 
            this.playerCard5.Location = new System.Drawing.Point(260, 333);
            this.playerCard5.Name = "playerCard5";
            this.playerCard5.Size = new System.Drawing.Size(100, 140);
            this.playerCard5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerCard5.TabIndex = 28;
            this.playerCard5.TabStop = false;
            // 
            // playerCard4
            // 
            this.playerCard4.Location = new System.Drawing.Point(220, 333);
            this.playerCard4.Name = "playerCard4";
            this.playerCard4.Size = new System.Drawing.Size(100, 140);
            this.playerCard4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerCard4.TabIndex = 27;
            this.playerCard4.TabStop = false;
            // 
            // playerCard3
            // 
            this.playerCard3.Location = new System.Drawing.Point(185, 333);
            this.playerCard3.Name = "playerCard3";
            this.playerCard3.Size = new System.Drawing.Size(100, 140);
            this.playerCard3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerCard3.TabIndex = 26;
            this.playerCard3.TabStop = false;
            // 
            // playerCard2
            // 
            this.playerCard2.Location = new System.Drawing.Point(150, 333);
            this.playerCard2.Name = "playerCard2";
            this.playerCard2.Size = new System.Drawing.Size(100, 140);
            this.playerCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerCard2.TabIndex = 25;
            this.playerCard2.TabStop = false;
            // 
            // playerCard1
            // 
            this.playerCard1.Location = new System.Drawing.Point(115, 333);
            this.playerCard1.Name = "playerCard1";
            this.playerCard1.Size = new System.Drawing.Size(100, 140);
            this.playerCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerCard1.TabIndex = 24;
            this.playerCard1.TabStop = false;
            // 
            // dealerCard5
            // 
            this.dealerCard5.Location = new System.Drawing.Point(400, 22);
            this.dealerCard5.Name = "dealerCard5";
            this.dealerCard5.Size = new System.Drawing.Size(100, 140);
            this.dealerCard5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.dealerCard5.TabIndex = 23;
            this.dealerCard5.TabStop = false;
            // 
            // dealerCard4
            // 
            this.dealerCard4.Location = new System.Drawing.Point(365, 22);
            this.dealerCard4.Name = "dealerCard4";
            this.dealerCard4.Size = new System.Drawing.Size(100, 140);
            this.dealerCard4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.dealerCard4.TabIndex = 22;
            this.dealerCard4.TabStop = false;
            // 
            // dealerCard3
            // 
            this.dealerCard3.Location = new System.Drawing.Point(330, 22);
            this.dealerCard3.Name = "dealerCard3";
            this.dealerCard3.Size = new System.Drawing.Size(100, 140);
            this.dealerCard3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.dealerCard3.TabIndex = 21;
            this.dealerCard3.TabStop = false;
            // 
            // dealerCard2
            // 
            this.dealerCard2.Location = new System.Drawing.Point(295, 22);
            this.dealerCard2.Name = "dealerCard2";
            this.dealerCard2.Size = new System.Drawing.Size(100, 140);
            this.dealerCard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.dealerCard2.TabIndex = 20;
            this.dealerCard2.TabStop = false;
            // 
            // dealerCard1
            // 
            this.dealerCard1.Location = new System.Drawing.Point(260, 22);
            this.dealerCard1.Name = "dealerCard1";
            this.dealerCard1.Size = new System.Drawing.Size(100, 140);
            this.dealerCard1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.dealerCard1.TabIndex = 19;
            this.dealerCard1.TabStop = false;
            // 
            // backgroundImage
            // 
            this.backgroundImage.Image = global::CSC460_BlackJack.Properties.Resources.Background;
            this.backgroundImage.Location = new System.Drawing.Point(-2, 0);
            this.backgroundImage.Name = "backgroundImage";
            this.backgroundImage.Size = new System.Drawing.Size(639, 541);
            this.backgroundImage.TabIndex = 37;
            this.backgroundImage.TabStop = false;
            // 
            // chipBetAmount
            // 
            this.chipBetAmount.Location = new System.Drawing.Point(115, 242);
            this.chipBetAmount.Name = "chipBetAmount";
            this.chipBetAmount.Size = new System.Drawing.Size(60, 60);
            this.chipBetAmount.TabIndex = 38;
            this.chipBetAmount.TabStop = false;
            // 
            // chipBetSplit
            // 
            this.chipBetSplit.Location = new System.Drawing.Point(379, 243);
            this.chipBetSplit.Name = "chipBetSplit";
            this.chipBetSplit.Size = new System.Drawing.Size(60, 60);
            this.chipBetSplit.TabIndex = 39;
            this.chipBetSplit.TabStop = false;
            this.chipBetSplit.Visible = false;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 536);
            this.Controls.Add(this.chipBetSplit);
            this.Controls.Add(this.chipBetAmount);
            this.Controls.Add(this.playerSplit5);
            this.Controls.Add(this.playerSplit4);
            this.Controls.Add(this.playerSplit3);
            this.Controls.Add(this.playerSplit2);
            this.Controls.Add(this.playerSplit1);
            this.Controls.Add(this.splitTotalLabel);
            this.Controls.Add(this.splitButton);
            this.Controls.Add(this.doubleDownButton);
            this.Controls.Add(this.playerCard5);
            this.Controls.Add(this.playerCard4);
            this.Controls.Add(this.playerCard3);
            this.Controls.Add(this.playerCard2);
            this.Controls.Add(this.playerCard1);
            this.Controls.Add(this.dealerCard5);
            this.Controls.Add(this.dealerCard4);
            this.Controls.Add(this.dealerCard3);
            this.Controls.Add(this.dealerCard2);
            this.Controls.Add(this.dealerCard1);
            this.Controls.Add(this.playerName);
            this.Controls.Add(this.betAmountBox);
            this.Controls.Add(this.chipCount);
            this.Controls.Add(this.newGameButton);
            this.Controls.Add(this.dealerTotal);
            this.Controls.Add(this.playerTotal);
            this.Controls.Add(this.stayButton);
            this.Controls.Add(this.hitButton);
            this.Controls.Add(this.backgroundImage);
            this.Name = "Game";
            this.Text = "Blackjack";
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerSplit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dealerCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chipBetAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chipBetSplit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button hitButton;
        private System.Windows.Forms.Button stayButton;
        private System.Windows.Forms.Label playerTotal;
        private System.Windows.Forms.Label dealerTotal;
        private System.Windows.Forms.Button newGameButton;
        private System.Windows.Forms.Label chipCount;
        private System.Windows.Forms.ComboBox betAmountBox;
        private System.Windows.Forms.Label playerName;
        private System.Windows.Forms.PictureBox dealerCard1;
        private System.Windows.Forms.PictureBox dealerCard2;
        private System.Windows.Forms.PictureBox dealerCard3;
        private System.Windows.Forms.PictureBox dealerCard4;
        private System.Windows.Forms.PictureBox dealerCard5;
        private System.Windows.Forms.PictureBox playerCard1;
        private System.Windows.Forms.PictureBox playerCard2;
        private System.Windows.Forms.PictureBox playerCard3;
        private System.Windows.Forms.PictureBox playerCard4;
        private System.Windows.Forms.PictureBox playerCard5;
        private System.Windows.Forms.Button doubleDownButton;
        private System.Windows.Forms.Button splitButton;
        private System.Windows.Forms.PictureBox playerSplit4;
        private System.Windows.Forms.PictureBox playerSplit3;
        private System.Windows.Forms.PictureBox playerSplit2;
        private System.Windows.Forms.PictureBox playerSplit5;
        private System.Windows.Forms.PictureBox playerSplit1;
        private System.Windows.Forms.Label splitTotalLabel;
        private System.Windows.Forms.PictureBox backgroundImage;
        private System.Windows.Forms.PictureBox chipBetAmount;
        private System.Windows.Forms.PictureBox chipBetSplit;

    }
}

