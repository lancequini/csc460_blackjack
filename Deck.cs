﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC460_BlackJack
{
    class Deck
    {
        private List<Card> deck;
        private int deckCutLine; 

        /*
         * Initializes the deck. 
         * 
         * Creates the deckCutLine to be used to show when to start a new deck.
         */
        public Deck()
        {
            deck = new List<Card>();
            loadDeck();
        }

        /*
         * Creates a new deck
         */ 
        public void resetDeck()
        {
            clearDeck();
            loadDeck();
            shuffleDeck();
        }

        /*
         * Clears the deck
         */
        private void clearDeck()
        {
            deck = new List<Card>();
        }

        /*
         * Loads the deck, cards are in order from 1 - 13 of suit.
         */
        private void loadDeck() 
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    deck.Add(new Card(i, j));
                }
            }

            shuffleDeck();
        }
        
        /**
         * Suffles the deck into random order.
         * 
         * The deck must have been initialized and loaded before it can be shuffled.
         */
        private void shuffleDeck() 
        {
            Random r = new Random();
            int n = deck.Count;
            while (n > 1)
            {
                n--;
                int k = r.Next(n + 1);
                Card card = deck[k];
                deck[k] = deck[n];
                deck[n] = card;
            }

            addCutLine();
        }

        /*
         * Draws the top card from the deck.
         * 
         * After the top card has been draw, it is removed from the deck and returned.
         */
        public Card drawCard() 
        {
            Card returnCard = deck[deck.Count - 1];

            removeTopCard();

            return returnCard;
        }

        /*
         * Removes the top card from the deck.
         * 
         */
        private void removeTopCard() 
        {
            List<Card> tempDeck = new List<Card>();

            for(int i = 0; i < deck.Count - 1; i ++){
                tempDeck.Add(deck[i]);
            }

            cutLinePassed();

            deck = tempDeck;
        }

        /*
         * Returns the number of cards remaining in the deck
         */
        public int getDeckCount() 
        {
            return deck.Count;
        }

        /*
         * Adds the Cut Line to the deck.
         */
        private void addCutLine()
        {
            Random r = new Random();
            deckCutLine = r.Next(20) + 10;
        }


        /*
         * Checks to see if the deckCount has reached the deckCutLine
         * 
         */
        public Boolean cutLinePassed() 
        {
            if (deck.Count <= deckCutLine) 
            {
                return true;          
            }
            else
            {
                return false;
            }
        }
    }
}
