﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC460_BlackJack
{
    [Serializable()]
    public class Card
    {
        private int cardSuit;
        private int cardFaceValue;
        private int cardValue;

        /*
         * Initializes the card. Requires a cardSuite and a cardFaceValue
         * 
         * The cardFaceValue is a 1-13 value representing the 
         */
        public Card(int cardSuit, int cardFaceValue)
        {
            this.cardSuit = cardSuit;
            this.cardFaceValue = cardFaceValue;


            switch(cardFaceValue)
            {
                case 0:
                    cardValue = 11;
                    break;
                case 1:
                    cardValue = 2;
                    break;
                case 2:
                    cardValue = 3;
                    break;
                case 3:
                    cardValue = 4;
                    break;
                case 4:
                    cardValue = 5;
                    break;
                case 5:
                    cardValue = 6;
                    break;
                case 6:
                    cardValue = 7;
                    break;
                case 7:
                    cardValue = 8;
                    break;
                case 8:
                    cardValue = 9;
                    break;
                case 9:
                    cardValue = 10;
                    break;
                case 10:
                    cardValue = 10;
                    break;
                case 11:
                    cardValue = 10;
                    break;
                case 12:
                    cardValue = 10;
                    break;
                default:
                    cardValue = 2;
                    break;
            }
        }

        /*
         * Returns the suit of card.
         */
        public String getCardSuit()
        {
            switch (cardSuit)
            {
                case 0:
                    return "Spades";
                case 1:
                    return "Clubs";
                case 2:
                    return "Diamonds";
                case 3:
                    return "Hearts";
                default:
                    return "Diamonds";

            }
        }


        /*
         * Returns the cardValue for the card. 
         * 
         * Holds the actual value when calculating the wieght of the card. 
         */
        public int getCardValue() 
        {
            return cardValue;
        }


        /*
         * Returns the string representation of the cardFaceValue
         */
        public string getCardValueString() 
        {
            switch (cardFaceValue)
            {
                case 0:
                    return "Ace";
                case 1:
                    return "Two";
                case 2:
                    return "Three";
                case 3:
                    return "Four";
                case 4:
                    return "Five";
                case 5:
                    return "Six";
                case 6:
                    return "Seven";
                case 7:
                    return "Eight";
                case 8:
                    return "Nine";
                case 9:
                    return "Ten";
                case 10:
                    return "Jack";
                case 11:
                    return "Queen";
                case 12:
                    return "King";
                default:
                    return "Two";
            }
        }

        public Image GetCardImage()
        {
            //sets the filepath to be the cards folder in the project directory
            String filepath = System.Reflection.Assembly.GetEntryAssembly().Location + @"\..\..\..\Blackjack\Blackjack\Cards\";

            switch (cardSuit)
            {
                case 0:
                    filepath = filepath + "s";
                    break;
                case 1:
                    filepath = filepath + "c";
                    break;
                case 2:
                    filepath = filepath + "d";
                    break;
                case 3:
                    filepath = filepath + "h";
                    break;
                default:
                    break;
            }

            switch (cardFaceValue)
            {
                case 0:
                    filepath = filepath + "01";
                    break;
                case 1:
                    filepath = filepath + "02";
                    break;
                case 2:
                    filepath = filepath + "03";
                    break;
                case 3:
                    filepath = filepath + "04";
                    break;
                case 4:
                    filepath = filepath + "05";
                    break;
                case 5:
                    filepath = filepath + "06";
                    break;
                case 6:
                    filepath = filepath + "07";
                    break;
                case 7:
                    filepath = filepath + "08";
                    break;
                case 8:
                    filepath = filepath + "09";
                    break;
                case 9:
                    filepath = filepath + "10";
                    break;
                case 10:
                    filepath = filepath + "11";
                    break;
                case 11:
                    filepath = filepath + "12";
                    break;
                case 12:
                    filepath = filepath + "13";
                    break;
                default:
                    filepath = filepath + "02";
                    break;
            }

            filepath = filepath + ".png";

            return Image.FromFile(filepath);
        }

        public String getString()
        {
            String toReturn = "";

            toReturn = getCardValueString() + " of " + getCardSuit();

            return toReturn;
        }
    }
}
