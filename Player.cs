﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CSC460_BlackJack
{   [Serializable()]
    public class Player
    {
        private string playerName;
        private int chipCount;
        private int overallTotal;
        private int handsWon, handsLost, blackjacks;
        private List<Card> playerHand;
        private List<Card> playerSplitHand;
        private int betAmount;
        private int chipsBought;

        public Player() 
        {
            playerName = "Player";
            chipCount = 100;
            overallTotal = +0;
            handsWon = 0;
            handsLost = 0;
            blackjacks = 0;
            chipsBought = 100;
            resetHand();
        }

        public Player(String name) 
        {
            playerName = name;
            chipCount = 100;
            overallTotal = 0;
            handsWon = 0;
            handsLost = 0;
            blackjacks = 0;
            chipsBought = 100;
            resetHand();
        }

        public Player(String name, int chipCount, int overallTotal, int hWon, int hLost, int blackjacks, int chipsBought)
        {
            playerName = name;
            this.chipCount = chipCount;
            this.overallTotal = overallTotal;
            handsWon = hWon;
            handsLost = hLost;
            this.blackjacks = blackjacks;
            this.chipsBought = chipsBought;
            resetHand();
        }

        /*
         * Returns the player's name.
         */
        public String getName() 
        {
            return playerName;
        }

        /*
         * Gets the current chip count for the player.
         */
        public int getChipCount()
        {
            return chipCount;
        }

        /*
         * Returns the overall total of the player's money.
         */
        public int getOverallTotal()
        {
            return overallTotal;
        }

        /*
         * Adds a card to the player's hand.
         */
        public void addCard(Card card) 
        {
            playerHand.Add(card);
        }

        /*
         * Adds a card to the player's split hand.
         */
        public void addSplitCard(Card card)
        {
            playerSplitHand.Add(card);
        }

        /*
         * Resets the player's hand to a new blank one for cards to be added to.
         * Resets the player's split hand to a new blank hand.
         * 
         * This should be used at the start of every new game.
         */
        public void resetHand()
        {
            playerHand = new List<Card>();
            playerSplitHand = new List<Card>();
        }

        /*
         * Returns the player's hand.
         */
        public List<Card> getHand()
        {
            return playerHand;
        }

        /*
         * Returns the player's split hand.
         */
        public List<Card> getSplitHand()
        {
            return playerSplitHand;
        }



        /*
         * Returns total count of the player's hand.
         */
        public int getHandTotal() 
        {
            bool handleAce = false;
            int countAce = 0;
            int total = 0;
            if (playerHand.Count > 0)
            {
                for (int i = 0; i < playerHand.Count; i++)
                {
                    if (playerHand[i].getCardValue() == 11)
                    {
                        handleAce = true;
                        countAce++;
                    }
                    total += playerHand[i].getCardValue();
                }

                if (handleAce)
                {
                    total = aceHandle(total, countAce);
                }
            }
            return total;
        }

        public int getSplitHandTotal()
        {
            bool handleAce = false;
            int countAce = 0;
            int total = 0;
            if (playerSplitHand.Count > 0)
            {
                for (int i = 0; i < playerSplitHand.Count; i++)
                {
                    if (playerSplitHand[i].getCardValue() == 11)
                    {
                        handleAce = true;
                        countAce++;
                    }
                    total += playerSplitHand[i].getCardValue();
                }

                if (handleAce)
                {
                    total = aceHandle(total, countAce);
                }
            }
            return total;
        }

        /*
         * Handles Aces are 1 and 11
         */
        private int aceHandle(int total, int aceCount)
        {
            for (int i = 0; i < aceCount; i++)
            {
                if (total > 21)
                    {
                        total -= 10;
                    }
            }
            return total;                
            
        }


        /*
         * Subtracts the bet amount from the player's current chip count.
         */
        public void placeBet(int betAmount)
        {
            this.betAmount = betAmount;
            chipCount = chipCount - betAmount;
        }


        /*
         * Handles the bets when a player double downs.
         */
        public void doubleDown()
        {
            chipCount -= betAmount;
            betAmount += betAmount;
        }

        /*
         * Used to handle player split
         */
        public void playerSplit()
        {
            chipCount -= betAmount;
            Card toMove = playerHand[1];
            playerHand.Remove(toMove);
            playerSplitHand.Add(toMove);
        }

        /*
         * Returns the bet amount made by the player.
         */
        public int getBet()
        {
            return betAmount;
        }

        /*
         * Gets the current win percentage for the player.
         */
        public float getWinPercentage() 
        {
            float percentage = (float)handsWon / ((float)handsWon + (float)handsLost) * 100f;
            return percentage;
        }

        /*
         * Adds a win to handsWon
         */
        public void addWin(int betAmount)
        {
            handsWon++;
            chipCount = chipCount + betAmount;

            /*
            if ((betAmount / 2) == this.betAmount)
            {
                overallTotal += this.betAmount;
            }
            if ((betAmount - this.betAmount) == this.betAmount / 2)
            {
                overallTotal += (this.betAmount + (this.betAmount / 2));
            }*/

            overallTotal = chipCount - chipsBought;
        }

        /*
         * Returns the number of wins
         */
        public int getWins()
        {
            return handsWon;
        }


        /*
         * Adds a loss to handsLoss
         */
        public void addLoss()
        {
            handsLost++;
            //overallTotal -= betAmount;
            overallTotal = chipCount - chipsBought;
        }

        /*
         * Returns the number of losses
         */
        public int getLosses()
        {
            return handsLost;
        }


        /*
         * Returns the number of blackjacks the player has
         */
        public int getBlackJacks()
        {
            return blackjacks;
        }

        /*
         * Adds a blackjack to blackjacks
         */
        public void addBlackjack()
        {
            blackjacks++;
        }

        /*
         * Allows the player to buy in when their chip count reaches 0;
         */
        public void buyIn()
        {
            chipCount += 100;
            //overallTotal -= 100;
            chipsBought += 100;
        }
    }
}
