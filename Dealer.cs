﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC460_BlackJack
{
    class Dealer
    {
        private List<Card> dealerHand;
        public Dealer()
        {
            resetHand();
        }

        /*
         * Resets the dealer's hand.
         */
        public void resetHand()
        {
            dealerHand = new List<Card>();
        }

        /*
         * Adds a card to the dealer's hand.
         */
        public void addCard(Card card)
        {
            dealerHand.Add(card);
        }

        /*
         * Returns the dealer's hand
         */ 
        public List<Card> getHand()
        {
            return dealerHand;
        }

        /*
         * Returns total count of the player's hand.
         */
        public int getHandTotal()
        {
            bool handleAce = false;
            int countAce = 0;
            int total = 0;
            if (dealerHand.Count > 0)
            {
                for (int i = 0; i < dealerHand.Count; i++)
                {
                    if (dealerHand[i].getCardValue() == 11)
                    {
                        handleAce = true;
                        countAce++;
                    }
                    total += dealerHand[i].getCardValue();
                }

                if (handleAce)
                {
                    total = aceHandle(total, countAce);
                }
            }
            return total;
        }

        /*
         * Handles Aces are 1 and 11
         */
        private int aceHandle(int total, int aceCount)
        {
            for (int i = 0; i < aceCount; i++)
            {
                if (total > 21)
                {
                    total -= 10;
                }
            }
            return total;

        }

        /*
         * Returns the dealer's card that needs to be turned up.
         */
        public Card getFaceUpCard()
        {
            return dealerHand[1];
        }
    }
}
